package main

import (
	"encoding/json"
	"fmt"

	// "io"
	"io/ioutil"
	"log"
	"net/http"
	"net/smtp"

	"github.com/hako/durafmt"

	//"reflect"
	"bytes"
	"mime/multipart"
	"strconv"
	"strings"
	"time"
)

// Sites ...
type Sites struct {
	URL  string  `json:"url"`
	Name string  `json:"name"`
	User float64 `json:"user"`
}

// Token ...
type Token struct {
	Token string `json:"token"`
}

// User ...
type User struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Access   int    `json:"access"`
}

var tokenTime time.Time
var tokenAge time.Duration
var tokenData Token
var token string
var email string

// PREPARE INPUTS (sites)
func getData(file string) map[string]interface{} {

	var urls interface{}

	data, err := ioutil.ReadFile(file)

	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &urls)

	sites := urls.(map[string]interface{})

	return sites
}

// Get the desired config value from the config file
func getConfig(option string) string {

	var configdata map[string]interface{}
	configdata = getData("config.json")
	config := make(map[string]string)

	for _, configOption := range configdata {

		configOption := configOption.(map[string]interface{})

		for optName, optValue := range configOption {
			optValue := optValue.(string)
			config[optName] = optValue
		}

	}
	return config[option]
}

// Convert the interval string to the time.Duration type
func getInterval(interval string) time.Duration {
	i, err := strconv.ParseInt(interval, 10, 64)

	if err != nil {
		panic(err)
	}

	var timeInterval time.Duration
	timeInterval = time.Duration(i) * time.Second

	return timeInterval
}

// PREPARE INPUTS (sites)
func getSitesFile(file string) []Sites {

	var sites []Sites

	data, err := ioutil.ReadFile(file)

	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &sites)

	return sites
}

func apiLogin(apiUser string, apiPass string) string {

	tokenAge = time.Since(tokenTime)

	if tokenAge > time.Duration(time.Hour*72) {
		url := getConfig("api_url") + "/login"
		method := "POST"

		payload := &bytes.Buffer{}
		writer := multipart.NewWriter(payload)
		_ = writer.WriteField("user", apiUser)
		_ = writer.WriteField("pass", apiPass)
		err := writer.Close()
		if err != nil {
			fmt.Println(err)
		}

		client := &http.Client{}
		req, err := http.NewRequest(method, url, payload)

		if err != nil {
			fmt.Println(err)
		}
		req.Header.Set("Content-Type", writer.FormDataContentType())
		res, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)

		err = json.Unmarshal(body, &tokenData)

		tokenTime = time.Now()
		token = tokenData.Token
	}
	return token
}

func apiUserEmail(apiURL string, apiToken string, userID int) string {

	id := strconv.Itoa(userID)

	zpingerClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}
	log.Println(id)
	req, err := http.NewRequest(http.MethodGet, apiURL+"/api/user/"+id, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "zenterial-zpinger")

	req.Header.Add("Authorization", "Bearer "+apiToken)

	res, getErr := zpingerClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	// if body != nil {
	// 	defer body.Close()
	// }

	var user User

	jsonErr := json.Unmarshal([]byte(body), &user)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	userEmail := user.Email

	log.Println(userEmail)

	return userEmail
}

// Below we return a new type []Sites, a struct, when getting data from our API, but it's likely not gonna work with our getStatus function, so it should be adapted, and config.json format should then be adapted as well to follow the same structure of Sites struct for everything to be supported.

func getSitesAPI(apiURL string, apiToken string) []Sites {

	zpingerClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}

	req, err := http.NewRequest(http.MethodGet, apiURL+"/api/site", nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "zenterial-zpinger")

	req.Header.Add("Authorization", "Bearer "+apiToken)

	res, getErr := zpingerClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	// if body != nil {
	// 	defer body.Close()
	// }

	var sites []Sites

	jsonErr := json.Unmarshal([]byte(body), &sites)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	return sites
}

// Work in progress - sets site's laststatus on the API:
// func updateLastStatus(siteID uint, lastStatus bool, apiURL string, apiToken string) {
// 	zpingerClient := http.Client{
// 		Timeout: time.Second * 2, // Maximum of 2 secs
// 	}

// 	req, err := http.NewRequest(http.MethodPost, apiURL+"/api/site/"+string(siteID), nil)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	req.Header.Set("User-Agent", "zenterial-zpinger")

// 	req.Header.Add("Authorization", "Bearer "+apiToken)

// 	res, getErr := zpingerClient.Do(req)
// 	if getErr != nil {
// 		log.Fatal(getErr)
// 	}

// 	body, readErr := ioutil.ReadAll(res.Body)
// 	if readErr != nil {
// 		log.Fatal(readErr)
// 	}

// 	log.Println(body)
// }

// PROCESS URLs
func pingit(url string) string {

	var response string
	req, _ := http.NewRequest("GET", url, nil) // create a request and store it in req

	res, err := http.DefaultClient.Do(req) // do the request with a default client and store the response in res
	if err != nil {
		response = "site does not exist"
	} else {
		response = res.Status
	}

	return response // print body
}

// Send an email
func send(body string, subject string, email string) {
	from := getConfig("from_email")
	pass := getConfig("smtp_pass")
	to := email
	server := getConfig("smtp_server")
	port := getConfig("smtp_port")

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		subject +
		body

	err := smtp.SendMail(server+":"+port,
		smtp.PlainAuth("zPinger", from, pass, server),
		from, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return
	}

}

// Determine what to send and when depending on the status
func getStatus(interval time.Duration) {
	t := time.Tick(interval)
	var sites []Sites
	laststatus := make(map[string]string)
	crashtime := make(map[string]time.Time)
	downtime := make(map[string]time.Duration)
	sent := make(map[string]bool)
	stilldownsent := make(map[string]bool)

	for range t {

		if getConfig("api") != "false" {
			apiLogin(getConfig("api_user"), getConfig("api_pass"))
			sites = getSitesAPI(getConfig("api_url"), token)
		} else {
			sites = getSitesFile("sites.json")
			email = getConfig("alert_email")
		}
		log.Println(sites)

		// Loop through sites using its URL and siteOptions
		for _, site := range sites {

			if getConfig("api") != "false" {
				email = apiUserEmail(getConfig("api_url"), token, int(site.User))
			}
			// Check URL to get current status
			status := pingit(site.URL)

			// Define the body of the email
			output := "Site name: " + site.Name + "\n" + "URL: " + site.URL + "\nStatus: " + status + "\n\n"

			if status != "200 OK" {

				// Subject of the email if the site is down
				subject := "Subject: " + strings.Replace(site.URL, "https://", "", 1) + " down: " + status + "\n\n"

				// Send that email only if we haven't sent it already, and then mark it sent for this URL
				// We also set the time it went down as crashtime
				if sent[site.URL] == false {
					send(output, subject, email)
					log.Print(site.URL + " down - sent alert")
					crashtime[site.URL] = time.Now()
					sent[site.URL] = true
				} else {
					// if we already sent the email just add up the time since crashtime as downtime
					downtime[site.URL] = time.Since(crashtime[site.URL])
					log.Print(site.URL + " still down (" + durafmt.ParseShort(downtime[site.URL]).String() + ")")

					// If downtime is larger than 30 minutes send an email saying it's still down if we haven't sent that second message already
					if downtime[site.URL] > time.Duration(1.8e+12) && stilldownsent[site.URL] == false {
						subject := "Subject: " + strings.Replace(site.URL, "https://", "", 1) + " is still down: " + status + " (" + durafmt.ParseShort(downtime[site.URL]).String() + ")\n\n"
						send(output, subject, email)
						stilldownsent[site.URL] = true
					}
				}

				// If status is OK we check if it wasn't OK on last check in order to send that it is back up
				// We also reset sent and stilldownsent here to get back to normal state
			} else if status == "200 OK" && laststatus[site.URL] != "" && laststatus[site.URL] != "200 OK" {

				downtime[site.URL] = time.Since(crashtime[site.URL])
				subject := "Subject: " + strings.Replace(site.URL, "https://", "", 1) + " is back up: " + status + " | total downtime: " + durafmt.ParseShort(downtime[site.URL]).String() + "\n\n"
				send(output, subject, email)
				log.Print(site.URL + " is back up! | total downtime: " + durafmt.ParseShort(downtime[site.URL]).String())
				sent[site.URL] = false
				stilldownsent[site.URL] = false

			} else {

				log.Print(site.URL + " is up!")

			}

			laststatus[site.URL] = status
			log.Print(laststatus[site.URL])

		}
	}
}

func main() {

	interval := getInterval(getConfig("interval"))

	log.Println("Running status checks every " + durafmt.ParseShort(interval).String() + "...")

	getStatus(interval)

}
