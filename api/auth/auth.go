package auth

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
	"log"
	"math/rand"
	"net/smtp"
	"strconv"
	"time"
	"zenterial.com/zpinger/config"
	"zenterial.com/zpinger/database"
)

// User ...
type User struct {
	gorm.Model
	Username   string `json:"username"`
	Email      string `json:"email"`
	Password   string `json:"password"`
	Access     int    `json:"access"`
	ConfirmKey string `json:"confirmkey"`
}

// Login ...
func Login(c *fiber.Ctx) {
	user := c.FormValue("user")
	pass := c.FormValue("pass")

	var foundUser User

	validUsers := ValidUsers()

	for _, u := range validUsers {
		validUser := u.Username
		validPass := u.Password

		if user == validUser && pass == validPass {
			foundUser = u
		}
	}

	if foundUser.Username != "" {
		log.Println("Logged in ", foundUser.Username)
		// Create token
		token := jwt.New(jwt.SigningMethodHS256)

		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		claims["name"] = foundUser.Username
		claims["id"] = foundUser.ID
		claims["access"] = foundUser.Access
		if foundUser.Access == 10 {
			claims["admin"] = true
		} else {
			claims["admin"] = false
		}
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

		// Generate encoded token and send it as response.
		t, err := token.SignedString([]byte("secret"))
		if err != nil {
			c.SendStatus(fiber.StatusInternalServerError)
			return
		}
		c.JSON(fiber.Map{"token": t})

	} else {
		c.SendStatus(fiber.StatusUnauthorized)
		return
	}
}

// GetUserID ...
func GetUserID(c *fiber.Ctx) float64 {
	user := c.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	id := claims["id"].(float64)
	return id
}

// GetUserACL ...
func GetUserACL(c *fiber.Ctx) float64 {
	user := c.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	ACL := claims["access"].(float64)
	return ACL
}

// UserID ...
var UserID = GetUserID

func hasUserAccess(c *fiber.Ctx, id string) bool {
	userID := GetUserID(c)
	userAccess := GetUserACL(c)

	ID, err := strconv.ParseFloat(id, 64)

	if err != nil {
		log.Println(err)
	}

	if userAccess == 10 {
		return true
	}

	if userID == ID {
		return true
	}

	return false
}

func userExists(newUser User) bool {
	db := database.DBConn
	var User []User
	var exists bool
	db.Find(&User)

	for _, u := range User {
		if u.Username == newUser.Username || u.Email == newUser.Email {
			exists = true
		}
	}

	if exists == true {
		return true
	}

	return false
}

// GetUsers ...
func GetUsers(c *fiber.Ctx) {
	userAccess := GetUserACL(c)
	if userAccess == 10 {
		db := database.DBConn
		var Users []User
		db.Find(&Users)
		c.JSON(Users)
	} else {
		db := database.DBConn
		var Users []User
		var myUser []User
		var userID float64
		db.Find(&Users)
		userID = GetUserID(c)

		for _, u := range Users {
			if u.ID == uint(userID) {
				myUser = append(myUser, u)
			}
		}

		c.JSON(myUser)
	}
}

// ValidUsers ...
func ValidUsers() map[int]User {
	db := database.DBConn
	var Users []User
	ValidUsers := make(map[int]User)
	db.Find(&Users)

	for k, u := range Users {
		if u.Access > 0 {
			ValidUsers[k] = u
		}
	}

	return ValidUsers
}

// GetUser ...
func GetUser(c *fiber.Ctx) {
	id := c.Params("id")

	if hasUserAccess(c, id) {
		db := database.DBConn
		var User User
		db.Find(&User, id)
		c.JSON(User)
	} else {
		c.SendStatus(fiber.StatusUnauthorized)
		return
	}
}

// NewUser ...
func NewUser(c *fiber.Ctx) {
	db := database.DBConn
	User := new(User)

	if err := c.BodyParser(User); err != nil {
		c.Status(503).Send(err)
		return
	}

	rand := rand.Intn(999999999)

	User.ConfirmKey = strconv.Itoa(rand)

	if userExists(*User) == false {
		db.Create(&User)
		c.JSON(User)
		registerUser(*User)
	} else {
		c.Status(500).Send("Sorry, username or email already exists.")
		return
	}
}

func registerUser(u User) {
	from := config.GetConfig("from_email")
	pass := config.GetConfig("smtp_pass")
	to := u.Email
	server := config.GetConfig("smtp_server")
	port := config.GetConfig("smtp_port")
	activateURL := config.GetConfig("zpinger_url") + "/?confirm=" + u.ConfirmKey

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: Your zPinger registration confirmation\n\n" +
		"Please click the link below to confirm your new zPinger account: \n\n" + activateURL

	err := smtp.SendMail(server+":"+port,
		smtp.PlainAuth("zPinger", from, pass, server),
		from, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return
	}
}

// ConfirmUser ...
func ConfirmUser(c *fiber.Ctx) {
	ckey := c.Params("confirm")
	db := database.DBConn

	var user User
	db.Where("confirm_key = ?", ckey).First(&user)

	if user.Access != 1 && user.Username != "" {
		user.Access = 1
		db.Save(&user)
		c.JSON(user)
	} else {
		c.Status(500).Send("This account is already activated.")
		return
	}
}

// UpdateUser ...
func UpdateUser(c *fiber.Ctx) {
	id := c.Params("id")
	db := database.DBConn

	var user User
	db.First(&user, id)

	if hasUserAccess(c, id) {
		if err := c.BodyParser(&user); err != nil {
			c.Status(503).Send(err)
			return
		}
		if userExists(user) == true {
			db.Save(&user)
			c.JSON(user)
		} else {
			c.Status(500).Send("Sorry, username " + user.Username + " doesn't exist.")
			return
		}
	} else {
		c.SendStatus(fiber.StatusUnauthorized)
		return
	}
}

// DeleteUser ...
func DeleteUser(c *fiber.Ctx) {
	id := c.Params("id")

	if hasUserAccess(c, id) {
		db := database.DBConn

		var User User
		db.First(&User, id)
		if User.Username == "" {
			c.Status(500).Send("No User Found with ID")
			return
		}
		db.Delete(&User)
		c.Send("User Successfully deleted")
	} else {
		c.SendStatus(fiber.StatusUnauthorized)
		return
	}
}
