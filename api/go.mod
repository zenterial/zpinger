module zenterial.com/zpinger

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gofiber/cors v0.2.0
	github.com/gofiber/fiber v1.11.0
	github.com/gofiber/jwt v0.1.0
	github.com/hako/durafmt v0.0.0-20191009132224-3f39dc1ed9f4
	github.com/jinzhu/gorm v1.9.12
)
