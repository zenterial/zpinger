package main

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber"
	"github.com/gofiber/jwt" // jwtware
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	// "time"
	"github.com/gofiber/cors"
	"zenterial.com/zpinger/auth"
	"zenterial.com/zpinger/config"
	"zenterial.com/zpinger/database"
	"zenterial.com/zpinger/sites"
)

func initDatabase() {
	var err error

	dbuser := config.GetConfig("dbuser")
	dbhost := config.GetConfig("dbhost")
	dbpass := config.GetConfig("dbpass")
	dbname := config.GetConfig("dbname")

	database.DBConn, err = gorm.Open("mysql", dbuser+":"+dbpass+"@("+dbhost+":3306)/"+dbname+"?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		fmt.Println(err)
		panic("failed to connect database")

	}

	fmt.Println("Connection Opened to Database")

	database.DBConn.AutoMigrate(&sites.Site{}, &auth.User{})
	fmt.Println("Database Migrated")
}

func api(c *fiber.Ctx) {
	user := c.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	name := claims["name"].(string)
	c.Send("Welcome " + name)
}

func setupRoutes(app *fiber.App) {
	app.Get("/api/site", sites.GetSites)
	app.Get("/api/site/:id", sites.GetSite)
	app.Post("/api/site", sites.NewSite)
	app.Post("/api/site/:id", sites.UpdateSite)
	app.Delete("/api/site/:id", sites.DeleteSite)
	app.Get("/api/user", auth.GetUsers)
	app.Get("/api/user/:id", auth.GetUser)
	app.Post("/api/user/:id", auth.UpdateUser)
	app.Delete("/api/user/:id", auth.DeleteUser)
}

func main() {
	app := fiber.New()
	app.Use(cors.New())
	initDatabase()
	defer database.DBConn.Close()

	// Login route
	app.Post("/login", auth.Login)
	app.Post("/api/user", auth.NewUser)
	app.Post("/api/user/confirm/:confirm", auth.ConfirmUser)

	// JWT Middleware
	app.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte("secret"),
	}))

	// Restricted routes
	app.Get("/api", api)

	setupRoutes(app)

	app.Listen(3000)

}
