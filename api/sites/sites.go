package sites

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
	"zenterial.com/zpinger/auth"
	"zenterial.com/zpinger/database"
)

// Site ...
type Site struct {
	gorm.Model
	Name       string  `json:"name"`
	URL        string  `json:"url"`
	User       float64 `json:"user"`
	Server     string  `json:"server"`
	LastStatus bool    `json:"laststatus"`
}

func hasSiteAccess(c *fiber.Ctx, siteUser float64) bool {
	userID := auth.GetUserID(c)
	userAccess := auth.GetUserACL(c)

	if userAccess == 10 {
		return true
	}

	if userID == siteUser {
		return true
	}

	return false
}

func siteExists(newSite Site) bool {
	db := database.DBConn
	var Site []Site
	var exists bool
	db.Find(&Site)

	for _, s := range Site {
		if s.URL == newSite.URL {
			exists = true
		}
	}

	if exists == true {
		return true
	}

	return false
}

// GetSites ...
func GetSites(c *fiber.Ctx) {
	userAccess := auth.GetUserACL(c)
	if userAccess == 10 {
		db := database.DBConn
		var sites []Site
		db.Find(&sites)
		c.JSON(sites)
	} else {
		// c.SendStatus(fiber.StatusUnauthorized)
		// return
		db := database.DBConn
		var sites []Site
		var userSites []Site
		var userID float64
		db.Find(&sites)
		userID = auth.GetUserID(c)

		for _, s := range sites {
			if s.User == userID {
				userSites = append(userSites, s)
			}
		}

		c.JSON(userSites)
	}
}

// GetSite ...
func GetSite(c *fiber.Ctx) {
	id := c.Params("id")
	db := database.DBConn
	var site Site
	db.Find(&site, id)

	if hasSiteAccess(c, site.User) {
		c.JSON(site)
	} else {
		c.SendStatus(fiber.StatusUnauthorized)
		return
	}
}

// NewSite ...
func NewSite(c *fiber.Ctx) {
	db := database.DBConn
	site := new(Site)
	site.User = auth.GetUserID(c)

	if err := c.BodyParser(site); err != nil {
		c.Status(503).Send(err)
		return
	}

	if siteExists(*site) == false {
		db.Create(&site)
	} else {
		c.Status(500).Send("Sorry, site (URL) " + site.URL + " already exists.")
		return
	}

	c.JSON(site)
}

// UpdateSite ...
func UpdateSite(c *fiber.Ctx) {
	id := c.Params("id")
	db := database.DBConn

	var site Site
	db.First(&site, id)

	if hasSiteAccess(c, site.User) {
		if err := c.BodyParser(&site); err != nil {
			c.Status(503).Send(err)
			return
		}

		db.Save(&site)
		c.JSON(site)
	} else {
		c.SendStatus(fiber.StatusUnauthorized)
		return
	}
}

// DeleteSite ...
func DeleteSite(c *fiber.Ctx) {
	id := c.Params("id")
	db := database.DBConn

	var site Site
	db.First(&site, id)

	if hasSiteAccess(c, site.User) {
		if site.Name == "" {
			c.Status(500).Send("No Site Found with ID")
			return
		}
		db.Delete(&site)
		c.Send("Site Successfully deleted")
	} else {
		c.SendStatus(fiber.StatusUnauthorized)
		return
	}
}
