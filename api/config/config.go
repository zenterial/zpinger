package config

import (
	"encoding/json"
	"io/ioutil"
)

// GetData ...
func getData(file string) map[string]interface{} {

	var urls interface{}

	data, err := ioutil.ReadFile(file)

	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &urls)

	sites := urls.(map[string]interface{})

	return sites
}

// GetConfig ...
func GetConfig(option string) string {

	var configdata map[string]interface{}
	configdata = getData("config.json")
	config := make(map[string]string)

	for _, configOption := range configdata {

		configOption := configOption.(map[string]interface{})

		for optName, optValue := range configOption {
			optValue := optValue.(string)
			config[optName] = optValue
		}

	}
	return config[option]
}
