PROJECT OUTLINE

API - manages the data (sites, users, auth) in the db

UI - microservice written in Svelte

PINGER - uses the API sites list to monitor sites

TODO:

Frontend
  - Show green if last status wasn't failure, or red if it was (add laststatus: 1 or 0 to Sites struct)
  - Show registered message
  - Listen to API endpoints instead of requiring refresh to show changes
  - Showing errors coming from API? If code works might not be necessary if we already do validation.

API
  - Initial admin user mechanism (if no user create user 1 with access 10 and user/pass from ENV)

DEPLOYMENT:
  - Pipeline: automate the build/deploy process.

IDEAS:
  - Notifiers struct with slices for various different notifiers (slack, telegram, email..)
  - Notifiers slice in the user struct for enabling multiple possible notification channels
  - StatusLog struct tied to site by siteID
    - It stores only site failure statuses
    - API would calculate uptime percentage with a special function taking siteID as input
    - Front shows uptime percentage graph widget
    - Front also shows "Log" button opening a modal with the full status log


  